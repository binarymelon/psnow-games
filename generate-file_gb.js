// to be run on https://www.playstation.com/en-gb/explore/playstation-now/ps-now-games/

var downloadFunc = function(strData, strFileName) {
    var D = document,
        A = arguments,
        a = D.createElement("a"),
        d = A[0],
        n = A[1],
        t = A[2] || "text/plain";

    //build download link:
    a.href = "data:" + "charset=UTF-8," + strData;

    if (window.MSBlobBuilder) { // IE10
        var bb = new Blob(["\uFEFF" + buffer],
        { encoding: "UTF-8", type: "text/plain;charset=UTF-8" });
        bb.append("\uFEFF" + strData);
        return navigator.msSaveBlob(bb, strFileName);
    } /* end if(window.MSBlobBuilder) */

    if ('download' in a) { //FF20, CH19
        a.setAttribute("download", n);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            var e = D.createEvent("MouseEvents");
            e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
            D.body.removeChild(a);
        }, 66);
        return true;
    } /* end if('download' in a) */
};

var fileText = '';

var sections = [
  { "id" : 1, "name" : "PS4"},
  { "id" : 2, "name" : "PS3"},
  { "id" : 3, "name" : "PS2"}
];

for (var i = 1; i <= sections.length; i++) {
  var section = sections[i - 1];
  fileText = fileText + section.name + escape('\n');
  fileText = fileText + "==========================================" + escape('\n');
  $("div[id*=content-" + section.id + "]").find("div[id*=inlinetabs]").find("p:not(:empty)").each(function(i, elem) {
      if ($(elem).text().trim() !== "") {
          fileText = fileText + $(elem).text().trim() + escape('\n');
      }
    });
  if (i < sections.length) fileText = fileText + escape('\n');
}

downloadFunc(fileText.trim(), 'games-list_gb.txt');
