// to be run on https://www.playstation.com/en-us/explore/playstation-now/games/

var downloadFunc = function(strData, strFileName) {
    var D = document,
        A = arguments,
        a = D.createElement("a"),
        d = A[0],
        n = A[1],
        t = A[2] || "text/plain";

    //build download link:
    a.href = "data:" + "charset=UTF-8," + strData;

    if (window.MSBlobBuilder) { // IE10
        var bb = new Blob(["\uFEFF" + buffer],
        { encoding: "UTF-8", type: "text/plain;charset=UTF-8" });
        bb.append("\uFEFF" + strData);
        return navigator.msSaveBlob(bb, strFileName);
    } /* end if(window.MSBlobBuilder) */

    if ('download' in a) { //FF20, CH19
        a.setAttribute("download", n);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            var e = D.createEvent("MouseEvents");
            e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
            D.body.removeChild(a);
        }, 66);
        return true;
    } /* end if('download' in a) */
};

var fileText = '';

$('.game-title').each(function(i, elem) {
  var gameTitle = $(elem).text();
  if (gameTitle.endsWith(" (new)")) gameTitle = gameTitle.slice(0, -6);
  fileText = fileText + gameTitle;
  fileText = fileText + escape('\n');
});

downloadFunc(fileText, 'games-list.txt');
